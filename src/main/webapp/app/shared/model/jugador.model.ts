export interface IJugador {
  id?: number;
  nombre?: string;
  numero?: number;
  equipoId?: number;
}

export class Jugador implements IJugador {
  constructor(public id?: number, public nombre?: string, public numero?: number, public equipoId?: number) {}
}
