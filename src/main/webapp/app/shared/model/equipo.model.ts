import { IJugador } from 'app/shared/model/jugador.model';

export interface IEquipo {
  id?: number;
  nombre?: string;
  responsable?: string;
  telefonoResponsable?: number;
  pago?: number;
  equipos?: IJugador[];
}

export class Equipo implements IEquipo {
  constructor(
    public id?: number,
    public nombre?: string,
    public responsable?: string,
    public telefonoResponsable?: number,
    public pago?: number,
    public equipos?: IJugador[]
  ) {}
}
