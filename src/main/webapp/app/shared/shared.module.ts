import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ControlEquiposSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [ControlEquiposSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [ControlEquiposSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ControlEquiposSharedModule {
  static forRoot() {
    return {
      ngModule: ControlEquiposSharedModule
    };
  }
}
