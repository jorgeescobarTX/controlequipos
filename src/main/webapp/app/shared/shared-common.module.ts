import { NgModule } from '@angular/core';

import { ControlEquiposSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [ControlEquiposSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [ControlEquiposSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class ControlEquiposSharedCommonModule {}
