import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ControlEquiposSharedModule } from 'app/shared';
import {
  JugadorComponent,
  JugadorDetailComponent,
  JugadorUpdateComponent,
  JugadorDeletePopupComponent,
  JugadorDeleteDialogComponent,
  jugadorRoute,
  jugadorPopupRoute
} from './';

const ENTITY_STATES = [...jugadorRoute, ...jugadorPopupRoute];

@NgModule({
  imports: [ControlEquiposSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    JugadorComponent,
    JugadorDetailComponent,
    JugadorUpdateComponent,
    JugadorDeleteDialogComponent,
    JugadorDeletePopupComponent
  ],
  entryComponents: [JugadorComponent, JugadorUpdateComponent, JugadorDeleteDialogComponent, JugadorDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ControlEquiposJugadorModule {}
