export * from './jugador.service';
export * from './jugador-update.component';
export * from './jugador-delete-dialog.component';
export * from './jugador-detail.component';
export * from './jugador.component';
export * from './jugador.route';
