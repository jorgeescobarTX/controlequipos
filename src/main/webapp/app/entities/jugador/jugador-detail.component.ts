import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJugador } from 'app/shared/model/jugador.model';

@Component({
  selector: 'jhi-jugador-detail',
  templateUrl: './jugador-detail.component.html'
})
export class JugadorDetailComponent implements OnInit {
  jugador: IJugador;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ jugador }) => {
      this.jugador = jugador;
    });
  }

  previousState() {
    window.history.back();
  }
}
