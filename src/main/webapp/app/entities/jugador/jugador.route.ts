import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Jugador } from 'app/shared/model/jugador.model';
import { JugadorService } from './jugador.service';
import { JugadorComponent } from './jugador.component';
import { JugadorDetailComponent } from './jugador-detail.component';
import { JugadorUpdateComponent } from './jugador-update.component';
import { JugadorDeletePopupComponent } from './jugador-delete-dialog.component';
import { IJugador } from 'app/shared/model/jugador.model';

@Injectable({ providedIn: 'root' })
export class JugadorResolve implements Resolve<IJugador> {
  constructor(private service: JugadorService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IJugador> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Jugador>) => response.ok),
        map((jugador: HttpResponse<Jugador>) => jugador.body)
      );
    }
    return of(new Jugador());
  }
}

export const jugadorRoute: Routes = [
  {
    path: '',
    component: JugadorComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Jugadors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: JugadorDetailComponent,
    resolve: {
      jugador: JugadorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Jugadors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: JugadorUpdateComponent,
    resolve: {
      jugador: JugadorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Jugadors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: JugadorUpdateComponent,
    resolve: {
      jugador: JugadorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Jugadors'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const jugadorPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: JugadorDeletePopupComponent,
    resolve: {
      jugador: JugadorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Jugadors'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
