import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IJugador, Jugador } from 'app/shared/model/jugador.model';
import { JugadorService } from './jugador.service';
import { IEquipo } from 'app/shared/model/equipo.model';
import { EquipoService } from 'app/entities/equipo';

@Component({
  selector: 'jhi-jugador-update',
  templateUrl: './jugador-update.component.html'
})
export class JugadorUpdateComponent implements OnInit {
  isSaving: boolean;

  equipos: IEquipo[];

  editForm = this.fb.group({
    id: [],
    nombre: [],
    numero: [],
    equipoId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected jugadorService: JugadorService,
    protected equipoService: EquipoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ jugador }) => {
      this.updateForm(jugador);
    });
    this.equipoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEquipo[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEquipo[]>) => response.body)
      )
      .subscribe((res: IEquipo[]) => (this.equipos = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(jugador: IJugador) {
    this.editForm.patchValue({
      id: jugador.id,
      nombre: jugador.nombre,
      numero: jugador.numero,
      equipoId: jugador.equipoId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const jugador = this.createFromForm();
    if (jugador.id !== undefined) {
      this.subscribeToSaveResponse(this.jugadorService.update(jugador));
    } else {
      this.subscribeToSaveResponse(this.jugadorService.create(jugador));
    }
  }

  private createFromForm(): IJugador {
    return {
      ...new Jugador(),
      id: this.editForm.get(['id']).value,
      nombre: this.editForm.get(['nombre']).value,
      numero: this.editForm.get(['numero']).value,
      equipoId: this.editForm.get(['equipoId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJugador>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackEquipoById(index: number, item: IEquipo) {
    return item.id;
  }
}
