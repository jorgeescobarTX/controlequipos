import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJugador } from 'app/shared/model/jugador.model';
import { JugadorService } from './jugador.service';

@Component({
  selector: 'jhi-jugador-delete-dialog',
  templateUrl: './jugador-delete-dialog.component.html'
})
export class JugadorDeleteDialogComponent {
  jugador: IJugador;

  constructor(protected jugadorService: JugadorService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.jugadorService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'jugadorListModification',
        content: 'Deleted an jugador'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-jugador-delete-popup',
  template: ''
})
export class JugadorDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ jugador }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(JugadorDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.jugador = jugador;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/jugador', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/jugador', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
