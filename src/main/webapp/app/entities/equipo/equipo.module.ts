import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ControlEquiposSharedModule } from 'app/shared';
import {
  EquipoComponent,
  EquipoDetailComponent,
  EquipoUpdateComponent,
  EquipoDeletePopupComponent,
  EquipoDeleteDialogComponent,
  equipoRoute,
  equipoPopupRoute
} from './';

const ENTITY_STATES = [...equipoRoute, ...equipoPopupRoute];

@NgModule({
  imports: [ControlEquiposSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [EquipoComponent, EquipoDetailComponent, EquipoUpdateComponent, EquipoDeleteDialogComponent, EquipoDeletePopupComponent],
  entryComponents: [EquipoComponent, EquipoUpdateComponent, EquipoDeleteDialogComponent, EquipoDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ControlEquiposEquipoModule {}
