import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'equipo',
        loadChildren: () => import('./equipo/equipo.module').then(m => m.ControlEquiposEquipoModule)
      },
      {
        path: 'jugador',
        loadChildren: () => import('./jugador/jugador.module').then(m => m.ControlEquiposJugadorModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ControlEquiposEntityModule {}
