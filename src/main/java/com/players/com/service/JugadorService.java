package com.players.com.service;

import com.players.com.service.dto.JugadorDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.players.com.domain.Jugador}.
 */
public interface JugadorService {

    /**
     * Save a jugador.
     *
     * @param jugadorDTO the entity to save.
     * @return the persisted entity.
     */
    JugadorDTO save(JugadorDTO jugadorDTO);

    /**
     * Save a jugador.
     *
     * @param jugadorDTO the entity to save.
     * @return the persisted entity.
     */
    JugadorDTO partialSave(JugadorDTO jugadorDTO);

    /**
     * Get all the jugadors.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<JugadorDTO> findAll(Pageable pageable);


    /**
     * Get the "id" jugador.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<JugadorDTO> findOne(Long id);

    /**
     * Delete the "id" jugador.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
