package com.players.com.service.impl;

import com.players.com.service.JugadorService;
import com.players.com.domain.Jugador;
import com.players.com.repository.JugadorRepository;
import com.players.com.service.dto.JugadorDTO;
import com.players.com.service.mapper.JugadorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Jugador}.
 */
@Service
@Transactional
public class JugadorServiceImpl implements JugadorService {

    private final Logger log = LoggerFactory.getLogger(JugadorServiceImpl.class);

    private final JugadorRepository jugadorRepository;

    private final JugadorMapper jugadorMapper;

    public JugadorServiceImpl(JugadorRepository jugadorRepository, JugadorMapper jugadorMapper) {
        this.jugadorRepository = jugadorRepository;
        this.jugadorMapper = jugadorMapper;
    }

    /**
     * Save a jugador.
     *
     * @param jugadorDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public JugadorDTO save(JugadorDTO jugadorDTO) {
        log.debug("Request to save Jugador : {}", jugadorDTO);
        Jugador jugador = jugadorMapper.toEntity(jugadorDTO);
        jugador = jugadorRepository.save(jugador);
        return jugadorMapper.toDto(jugador);
    }

    /**
     * Save a jugador.
     *
     * @param jugadorDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public JugadorDTO partialSave(JugadorDTO jugadorDTO)
    {
        log.debug("Request to save Jugador : {}", jugadorDTO);
        Long id = jugadorDTO.getId();
        Optional<Jugador> jugadorDTOInData = jugadorRepository.findById(id);
        
        if (jugadorDTO.getNombre() == null)
        {
            jugadorDTO.setNombre(jugadorDTOInData.get().getNombre());
        }

        if (jugadorDTO.getNumero() == null)
        {
            jugadorDTO.setNumero(jugadorDTOInData.get().getNumero());
        }

        if (jugadorDTO.getEquipoId() == null)
        {
            jugadorDTO.setEquipoId(jugadorDTOInData.get().getEquipo().getId());
        }

        Jugador jugador = jugadorMapper.toEntity(jugadorDTO);
        jugador = jugadorRepository.save(jugador);
        return jugadorMapper.toDto(jugador);
    }

    /**
     * Get all the jugadors.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JugadorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Jugadors");
        return jugadorRepository.findAll(pageable)
            .map(jugadorMapper::toDto);
    }


    /**
     * Get one jugador by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<JugadorDTO> findOne(Long id) {
        log.debug("Request to get Jugador : {}", id);
        return jugadorRepository.findById(id)
            .map(jugadorMapper::toDto);
    }

    /**
     * Delete the jugador by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Jugador : {}", id);
        jugadorRepository.deleteById(id);
    }
}
