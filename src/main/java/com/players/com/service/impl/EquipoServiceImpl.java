package com.players.com.service.impl;

import com.players.com.service.EquipoService;
import com.players.com.domain.Equipo;
import com.players.com.repository.EquipoRepository;
import com.players.com.service.dto.EquipoDTO;
import com.players.com.service.mapper.EquipoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Equipo}.
 */
@Service
@Transactional
public class EquipoServiceImpl implements EquipoService {

    private final Logger log = LoggerFactory.getLogger(EquipoServiceImpl.class);

    private final EquipoRepository equipoRepository;

    private final EquipoMapper equipoMapper;

    public EquipoServiceImpl(EquipoRepository equipoRepository, EquipoMapper equipoMapper) {
        this.equipoRepository = equipoRepository;
        this.equipoMapper = equipoMapper;
    }

    /**
     * Save a equipo.
     *
     * @param equipoDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EquipoDTO save(EquipoDTO equipoDTO) {
        log.debug("Request to save Equipo : {}", equipoDTO);
        Equipo equipo = equipoMapper.toEntity(equipoDTO);
        equipo = equipoRepository.save(equipo);
        return equipoMapper.toDto(equipo);
    }

    /**
     * Get all the equipos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EquipoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Equipos");
        return equipoRepository.findAll(pageable)
            .map(equipoMapper::toDto);
    }


    /**
     * Get one equipo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EquipoDTO> findOne(Long id) {
        log.debug("Request to get Equipo : {}", id);
        return equipoRepository.findById(id)
            .map(equipoMapper::toDto);
    }

    /**
     * Delete the equipo by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Equipo : {}", id);
        equipoRepository.deleteById(id);
    }
}
