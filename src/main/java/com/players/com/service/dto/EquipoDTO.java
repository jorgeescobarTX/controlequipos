package com.players.com.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.players.com.domain.Equipo} entity.
 */
public class EquipoDTO implements Serializable {

    private Long id;

    @NotNull
    private String nombre;

    @NotNull
    private String responsable;

    @NotNull
    private Long telefonoResponsable;

    private Long pago;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Long getTelefonoResponsable() {
        return telefonoResponsable;
    }

    public void setTelefonoResponsable(Long telefonoResponsable) {
        this.telefonoResponsable = telefonoResponsable;
    }

    public Long getPago() {
        return pago;
    }

    public void setPago(Long pago) {
        this.pago = pago;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EquipoDTO equipoDTO = (EquipoDTO) o;
        if (equipoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), equipoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EquipoDTO{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", responsable='" + getResponsable() + "'" +
            ", telefonoResponsable=" + getTelefonoResponsable() +
            ", pago=" + getPago() +
            "}";
    }
}
