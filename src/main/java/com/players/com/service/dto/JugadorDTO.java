package com.players.com.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.players.com.domain.Jugador} entity.
 */
public class JugadorDTO implements Serializable {

    private Long id;

    private String nombre;

    private Integer numero;


    private Long equipoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Long getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(Long equipoId) {
        this.equipoId = equipoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JugadorDTO jugadorDTO = (JugadorDTO) o;
        if (jugadorDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jugadorDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JugadorDTO{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", numero=" + getNumero() +
            ", equipo=" + getEquipoId() +
            "}";
    }
}
