package com.players.com.service.mapper;

import com.players.com.domain.*;
import com.players.com.service.dto.JugadorDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Jugador} and its DTO {@link JugadorDTO}.
 */
@Mapper(componentModel = "spring", uses = {EquipoMapper.class})
public interface JugadorMapper extends EntityMapper<JugadorDTO, Jugador> {

    @Mapping(source = "equipo.id", target = "equipoId")
    JugadorDTO toDto(Jugador jugador);

    @Mapping(source = "equipoId", target = "equipo")
    Jugador toEntity(JugadorDTO jugadorDTO);

    default Jugador fromId(Long id) {
        if (id == null) {
            return null;
        }
        Jugador jugador = new Jugador();
        jugador.setId(id);
        return jugador;
    }
}
