package com.players.com.service.mapper;

import com.players.com.domain.*;
import com.players.com.service.dto.EquipoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Equipo} and its DTO {@link EquipoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EquipoMapper extends EntityMapper<EquipoDTO, Equipo> {


    @Mapping(target = "equipos", ignore = true)
    @Mapping(target = "removeEquipo", ignore = true)
    Equipo toEntity(EquipoDTO equipoDTO);

    default Equipo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Equipo equipo = new Equipo();
        equipo.setId(id);
        return equipo;
    }
}
