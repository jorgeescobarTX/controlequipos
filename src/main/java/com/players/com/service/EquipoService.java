package com.players.com.service;

import com.players.com.service.dto.EquipoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.players.com.domain.Equipo}.
 */
public interface EquipoService {

    /**
     * Save a equipo.
     *
     * @param equipoDTO the entity to save.
     * @return the persisted entity.
     */
    EquipoDTO save(EquipoDTO equipoDTO);

    /**
     * Get all the equipos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EquipoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" equipo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EquipoDTO> findOne(Long id);

    /**
     * Delete the "id" equipo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
