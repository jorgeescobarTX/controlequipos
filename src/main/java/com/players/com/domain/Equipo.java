package com.players.com.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Equipo.
 */
@Entity
@Table(name = "equipo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Equipo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @NotNull
    @Column(name = "responsable", nullable = false)
    private String responsable;

    @NotNull
    @Column(name = "telefono_responsable", nullable = false)
    private Long telefonoResponsable;

    @Column(name = "pago")
    private Long pago;

    @OneToMany(mappedBy = "equipo")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Jugador> equipos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Equipo nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getResponsable() {
        return responsable;
    }

    public Equipo responsable(String responsable) {
        this.responsable = responsable;
        return this;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Long getTelefonoResponsable() {
        return telefonoResponsable;
    }

    public Equipo telefonoResponsable(Long telefonoResponsable) {
        this.telefonoResponsable = telefonoResponsable;
        return this;
    }

    public void setTelefonoResponsable(Long telefonoResponsable) {
        this.telefonoResponsable = telefonoResponsable;
    }

    public Long getPago() {
        return pago;
    }

    public Equipo pago(Long pago) {
        this.pago = pago;
        return this;
    }

    public void setPago(Long pago) {
        this.pago = pago;
    }

    public Set<Jugador> getEquipos() {
        return equipos;
    }

    public Equipo equipos(Set<Jugador> jugadors) {
        this.equipos = jugadors;
        return this;
    }

    public Equipo addEquipo(Jugador jugador) {
        this.equipos.add(jugador);
        jugador.setEquipo(this);
        return this;
    }

    public Equipo removeEquipo(Jugador jugador) {
        this.equipos.remove(jugador);
        jugador.setEquipo(null);
        return this;
    }

    public void setEquipos(Set<Jugador> jugadors) {
        this.equipos = jugadors;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Equipo)) {
            return false;
        }
        return id != null && id.equals(((Equipo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Equipo{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", responsable='" + getResponsable() + "'" +
            ", telefonoResponsable=" + getTelefonoResponsable() +
            ", pago=" + getPago() +
            "}";
    }
}
