package com.players.com.web.rest;

import com.players.com.service.JugadorService;
import com.players.com.web.rest.errors.BadRequestAlertException;
import com.players.com.service.dto.JugadorDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.players.com.domain.Jugador}.
 */
@RestController
@RequestMapping("/api")
public class JugadorResource {

    private final Logger log = LoggerFactory.getLogger(JugadorResource.class);

    private static final String ENTITY_NAME = "jugador";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JugadorService jugadorService;

    public JugadorResource(JugadorService jugadorService) {
        this.jugadorService = jugadorService;
    }

    /**
     * {@code POST  /jugadors} : Create a new jugador.
     *
     * @param jugadorDTO the jugadorDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new jugadorDTO, or with status {@code 400 (Bad Request)} if the jugador has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/jugadors")
    public ResponseEntity<JugadorDTO> createJugador(@RequestBody JugadorDTO jugadorDTO) throws URISyntaxException {
        log.debug("REST request to save Jugador : {}", jugadorDTO);
        if (jugadorDTO.getId() != null) {
            throw new BadRequestAlertException("A new jugador cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JugadorDTO result = jugadorService.save(jugadorDTO);
        return ResponseEntity.created(new URI("/api/jugadors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /jugadors} : Updates an existing jugador.
     *
     * @param jugadorDTO the jugadorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated jugadorDTO,
     * or with status {@code 400 (Bad Request)} if the jugadorDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the jugadorDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/jugadors/{id}")
    public ResponseEntity<JugadorDTO> updateJugador(@PathVariable Long id, @RequestBody JugadorDTO jugadorDTO) throws URISyntaxException {
        log.debug("REST request to update Jugador : {}", jugadorDTO);
        
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if (id != jugadorDTO.getId())
        {
            throw new BadRequestAlertException("Id for ", ENTITY_NAME, " is not the same provided ");
        }

        if (jugadorService.findOne(id) == null)
        {
            //http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.6
            throw new BadRequestAlertException("Id for ", ENTITY_NAME, " doesn't exist  ");
        }


        jugadorDTO.setId(id);

        JugadorDTO result = jugadorService.save(jugadorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jugadorDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /jugadors} : Updates an existing jugador.
     *
     * @param jugadorDTO the jugadorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated jugadorDTO,
     * or with status {@code 400 (Bad Request)} if the jugadorDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the jugadorDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping("/jugadors/{id}")
    public ResponseEntity<JugadorDTO> updatePartialJugador(@PathVariable Long id, @RequestBody JugadorDTO jugadorDTO) throws URISyntaxException {
        log.debug("REST request to update Jugador : {}", jugadorDTO);
        
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if (id != jugadorDTO.getId())
        {
            throw new BadRequestAlertException("Id for ", ENTITY_NAME, " is not the same provided ");
        }

        if (jugadorService.findOne(id) == null)
        {
            //http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.6
            throw new BadRequestAlertException("Id for ", ENTITY_NAME, " doesn't exist  ");
        }
        
        jugadorDTO.setId(id);

        JugadorDTO result = jugadorService.partialSave(jugadorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jugadorDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /jugadors} : get all the jugadors.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of jugadors in body.
     */
    @GetMapping("/jugadors")
    public ResponseEntity<List<JugadorDTO>> getAllJugadors(Pageable pageable) {
        log.debug("REST request to get a page of Jugadors");
        Page<JugadorDTO> page = jugadorService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /jugadors/:id} : get the "id" jugador.
     *
     * @param id the id of the jugadorDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the jugadorDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/jugadors/{id}")
    public ResponseEntity<JugadorDTO> getJugador(@PathVariable Long id) {
        log.debug("REST request to get Jugador : {}", id);
        Optional<JugadorDTO> jugadorDTO = jugadorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(jugadorDTO);
    }

    /**
     * {@code DELETE  /jugadors/:id} : delete the "id" jugador.
     *
     * @param id the id of the jugadorDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/jugadors/{id}")
    public ResponseEntity<Void> deleteJugador(@PathVariable Long id) {
        log.debug("REST request to delete Jugador : {}", id);
        jugadorService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
