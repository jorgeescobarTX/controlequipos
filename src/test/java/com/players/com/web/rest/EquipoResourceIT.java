package com.players.com.web.rest;

import com.players.com.ControlEquiposApp;
import com.players.com.domain.Equipo;
import com.players.com.repository.EquipoRepository;
import com.players.com.service.EquipoService;
import com.players.com.service.dto.EquipoDTO;
import com.players.com.service.mapper.EquipoMapper;
import com.players.com.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.players.com.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EquipoResource} REST controller.
 */
@SpringBootTest(classes = ControlEquiposApp.class)
public class EquipoResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_RESPONSABLE = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSABLE = "BBBBBBBBBB";

    private static final Long DEFAULT_TELEFONO_RESPONSABLE = 1L;
    private static final Long UPDATED_TELEFONO_RESPONSABLE = 2L;
    private static final Long SMALLER_TELEFONO_RESPONSABLE = 1L - 1L;

    private static final Long DEFAULT_PAGO = 1L;
    private static final Long UPDATED_PAGO = 2L;
    private static final Long SMALLER_PAGO = 1L - 1L;

    @Autowired
    private EquipoRepository equipoRepository;

    @Autowired
    private EquipoMapper equipoMapper;

    @Autowired
    private EquipoService equipoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEquipoMockMvc;

    private Equipo equipo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EquipoResource equipoResource = new EquipoResource(equipoService);
        this.restEquipoMockMvc = MockMvcBuilders.standaloneSetup(equipoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Equipo createEntity(EntityManager em) {
        Equipo equipo = new Equipo()
            .nombre(DEFAULT_NOMBRE)
            .responsable(DEFAULT_RESPONSABLE)
            .telefonoResponsable(DEFAULT_TELEFONO_RESPONSABLE)
            .pago(DEFAULT_PAGO);
        return equipo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Equipo createUpdatedEntity(EntityManager em) {
        Equipo equipo = new Equipo()
            .nombre(UPDATED_NOMBRE)
            .responsable(UPDATED_RESPONSABLE)
            .telefonoResponsable(UPDATED_TELEFONO_RESPONSABLE)
            .pago(UPDATED_PAGO);
        return equipo;
    }

    @BeforeEach
    public void initTest() {
        equipo = createEntity(em);
    }

    @Test
    @Transactional
    public void createEquipo() throws Exception {
        int databaseSizeBeforeCreate = equipoRepository.findAll().size();

        // Create the Equipo
        EquipoDTO equipoDTO = equipoMapper.toDto(equipo);
        restEquipoMockMvc.perform(post("/api/equipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipoDTO)))
            .andExpect(status().isCreated());

        // Validate the Equipo in the database
        List<Equipo> equipoList = equipoRepository.findAll();
        assertThat(equipoList).hasSize(databaseSizeBeforeCreate + 1);
        Equipo testEquipo = equipoList.get(equipoList.size() - 1);
        assertThat(testEquipo.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testEquipo.getResponsable()).isEqualTo(DEFAULT_RESPONSABLE);
        assertThat(testEquipo.getTelefonoResponsable()).isEqualTo(DEFAULT_TELEFONO_RESPONSABLE);
        assertThat(testEquipo.getPago()).isEqualTo(DEFAULT_PAGO);
    }

    @Test
    @Transactional
    public void createEquipoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = equipoRepository.findAll().size();

        // Create the Equipo with an existing ID
        equipo.setId(1L);
        EquipoDTO equipoDTO = equipoMapper.toDto(equipo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEquipoMockMvc.perform(post("/api/equipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Equipo in the database
        List<Equipo> equipoList = equipoRepository.findAll();
        assertThat(equipoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = equipoRepository.findAll().size();
        // set the field null
        equipo.setNombre(null);

        // Create the Equipo, which fails.
        EquipoDTO equipoDTO = equipoMapper.toDto(equipo);

        restEquipoMockMvc.perform(post("/api/equipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipoDTO)))
            .andExpect(status().isBadRequest());

        List<Equipo> equipoList = equipoRepository.findAll();
        assertThat(equipoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkResponsableIsRequired() throws Exception {
        int databaseSizeBeforeTest = equipoRepository.findAll().size();
        // set the field null
        equipo.setResponsable(null);

        // Create the Equipo, which fails.
        EquipoDTO equipoDTO = equipoMapper.toDto(equipo);

        restEquipoMockMvc.perform(post("/api/equipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipoDTO)))
            .andExpect(status().isBadRequest());

        List<Equipo> equipoList = equipoRepository.findAll();
        assertThat(equipoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTelefonoResponsableIsRequired() throws Exception {
        int databaseSizeBeforeTest = equipoRepository.findAll().size();
        // set the field null
        equipo.setTelefonoResponsable(null);

        // Create the Equipo, which fails.
        EquipoDTO equipoDTO = equipoMapper.toDto(equipo);

        restEquipoMockMvc.perform(post("/api/equipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipoDTO)))
            .andExpect(status().isBadRequest());

        List<Equipo> equipoList = equipoRepository.findAll();
        assertThat(equipoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEquipos() throws Exception {
        // Initialize the database
        equipoRepository.saveAndFlush(equipo);

        // Get all the equipoList
        restEquipoMockMvc.perform(get("/api/equipos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(equipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].responsable").value(hasItem(DEFAULT_RESPONSABLE.toString())))
            .andExpect(jsonPath("$.[*].telefonoResponsable").value(hasItem(DEFAULT_TELEFONO_RESPONSABLE.intValue())))
            .andExpect(jsonPath("$.[*].pago").value(hasItem(DEFAULT_PAGO.intValue())));
    }
    
    @Test
    @Transactional
    public void getEquipo() throws Exception {
        // Initialize the database
        equipoRepository.saveAndFlush(equipo);

        // Get the equipo
        restEquipoMockMvc.perform(get("/api/equipos/{id}", equipo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(equipo.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.responsable").value(DEFAULT_RESPONSABLE.toString()))
            .andExpect(jsonPath("$.telefonoResponsable").value(DEFAULT_TELEFONO_RESPONSABLE.intValue()))
            .andExpect(jsonPath("$.pago").value(DEFAULT_PAGO.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingEquipo() throws Exception {
        // Get the equipo
        restEquipoMockMvc.perform(get("/api/equipos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEquipo() throws Exception {
        // Initialize the database
        equipoRepository.saveAndFlush(equipo);

        int databaseSizeBeforeUpdate = equipoRepository.findAll().size();

        // Update the equipo
        Equipo updatedEquipo = equipoRepository.findById(equipo.getId()).get();
        // Disconnect from session so that the updates on updatedEquipo are not directly saved in db
        em.detach(updatedEquipo);
        updatedEquipo
            .nombre(UPDATED_NOMBRE)
            .responsable(UPDATED_RESPONSABLE)
            .telefonoResponsable(UPDATED_TELEFONO_RESPONSABLE)
            .pago(UPDATED_PAGO);
        EquipoDTO equipoDTO = equipoMapper.toDto(updatedEquipo);

        restEquipoMockMvc.perform(put("/api/equipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipoDTO)))
            .andExpect(status().isOk());

        // Validate the Equipo in the database
        List<Equipo> equipoList = equipoRepository.findAll();
        assertThat(equipoList).hasSize(databaseSizeBeforeUpdate);
        Equipo testEquipo = equipoList.get(equipoList.size() - 1);
        assertThat(testEquipo.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testEquipo.getResponsable()).isEqualTo(UPDATED_RESPONSABLE);
        assertThat(testEquipo.getTelefonoResponsable()).isEqualTo(UPDATED_TELEFONO_RESPONSABLE);
        assertThat(testEquipo.getPago()).isEqualTo(UPDATED_PAGO);
    }

    @Test
    @Transactional
    public void updateNonExistingEquipo() throws Exception {
        int databaseSizeBeforeUpdate = equipoRepository.findAll().size();

        // Create the Equipo
        EquipoDTO equipoDTO = equipoMapper.toDto(equipo);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEquipoMockMvc.perform(put("/api/equipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Equipo in the database
        List<Equipo> equipoList = equipoRepository.findAll();
        assertThat(equipoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEquipo() throws Exception {
        // Initialize the database
        equipoRepository.saveAndFlush(equipo);

        int databaseSizeBeforeDelete = equipoRepository.findAll().size();

        // Delete the equipo
        restEquipoMockMvc.perform(delete("/api/equipos/{id}", equipo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Equipo> equipoList = equipoRepository.findAll();
        assertThat(equipoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Equipo.class);
        Equipo equipo1 = new Equipo();
        equipo1.setId(1L);
        Equipo equipo2 = new Equipo();
        equipo2.setId(equipo1.getId());
        assertThat(equipo1).isEqualTo(equipo2);
        equipo2.setId(2L);
        assertThat(equipo1).isNotEqualTo(equipo2);
        equipo1.setId(null);
        assertThat(equipo1).isNotEqualTo(equipo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EquipoDTO.class);
        EquipoDTO equipoDTO1 = new EquipoDTO();
        equipoDTO1.setId(1L);
        EquipoDTO equipoDTO2 = new EquipoDTO();
        assertThat(equipoDTO1).isNotEqualTo(equipoDTO2);
        equipoDTO2.setId(equipoDTO1.getId());
        assertThat(equipoDTO1).isEqualTo(equipoDTO2);
        equipoDTO2.setId(2L);
        assertThat(equipoDTO1).isNotEqualTo(equipoDTO2);
        equipoDTO1.setId(null);
        assertThat(equipoDTO1).isNotEqualTo(equipoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(equipoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(equipoMapper.fromId(null)).isNull();
    }
}
