/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ControlEquiposTestModule } from '../../../test.module';
import { JugadorDeleteDialogComponent } from 'app/entities/jugador/jugador-delete-dialog.component';
import { JugadorService } from 'app/entities/jugador/jugador.service';

describe('Component Tests', () => {
  describe('Jugador Management Delete Component', () => {
    let comp: JugadorDeleteDialogComponent;
    let fixture: ComponentFixture<JugadorDeleteDialogComponent>;
    let service: JugadorService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ControlEquiposTestModule],
        declarations: [JugadorDeleteDialogComponent]
      })
        .overrideTemplate(JugadorDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JugadorDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JugadorService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
